Please see documentation on github for Empathy MVC framework: https://github.com/mikejw/empathy/tree/master/docs.

The project here on GitLab hosts the docker images for the optional "base-docker" development enironment for Empathy that allows for quick switching between apps. This requires Docker and Ansible.

Please see https://github.com/mikejw/base-docker.
